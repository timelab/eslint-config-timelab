module.exports = {
  "env": {
    "browser": true,
    "node": true,
    "es6": true
  },
  "globals": {
    "$": true,
    "jQuery": true,
    // Testing globals
    "describe": true,
    "it": true,
    "afterEach": true
  },
  "extends": "eslint:recommended",
  "installedESLint": true,
  "parserOptions": {
    "ecmaFeatures": {
      "experimentalObjectRestSpread": true,
      "jsx": true
    },
    "sourceType": "module"
  },
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  "rules": {
    "indent": [
      "error",
      2
    ],
    "no-trailing-spaces": ["error"],
    "no-unneeded-ternary": ["error"],
    "no-whitespace-before-property": ["error"],
    "space-before-blocks": ["error"],
    "space-before-function-paren": ["error", "never"],
    "space-infix-ops": ["error"],
    "space-unary-ops": ["error"],
    "keyword-spacing": ["error", { "before": true, "after": true }],
    "comma-spacing": ["error", { "before": false, "after": true }],
    "comma-dangle": ["error", "always-multiline"],
    "array-bracket-spacing": ["error", "never"],
    "object-curly-spacing": ["error", "always"],
    "prefer-arrow-callback": ["error", { "allowNamedFunctions": true }],
    "quotes": [
      "error",
      "double",
      {
        "allowTemplateLiterals": true
      }
    ],
    "jsx-quotes": [
      "error",
      "prefer-double"
    ],
    "prefer-template": ["error"],
    "semi": [
      "error",
      "always"
    ],
    "no-multiple-empty-lines": [
      "error", { "max": 2, "maxBOF": 0, "maxEOF": 1 }
    ],
    "object-shorthand": ["error"],
    "key-spacing": [
      "error", { "mode": "minimum" }
    ],
    "no-var": [
      "error"
    ],
    "prefer-const": [
      "error"
    ],
    "camelcase": [
      "error"
    ]
  }
}
